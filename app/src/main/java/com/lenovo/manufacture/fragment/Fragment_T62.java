package com.lenovo.manufacture.fragment;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lenovo.manufacture.R;
import com.lenovo.manufacture.activity.TWebView;
import com.lenovo.manufacture.utils.HttpTools;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class Fragment_T62 extends Fragment {

    private WebView wv;

    public Fragment_T62() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint({"SetJavaScriptEnabled"})
    private void initWebView(View view) {

        this.wv = new TWebView(view.getContext(), null);
        ViewGroup viewParent = view.findViewById(R.id.cusOR);
        viewParent.addView(wv, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));

        wv.loadUrl("file:///android_asset/carOrder.html");
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                /* mWebView.showLog("test Log"); */
            }
        });


        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wv.setBackgroundColor(0);
        wv.requestFocus();
        wv.setInitialScale(25);
//        webView.addJavascriptInterface(new T7.JavaScriptInterface(this), "nativeMethod");
        WebSettings webSetting = wv.getSettings();
        webSetting.setAllowFileAccess(true);
        webSetting.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
//        webSetting.setSupportZoom(true);
//        webSetting.setBuiltInZoomControls(true);
        webSetting.setUseWideViewPort(true);
        webSetting.setSupportMultipleWindows(false);
//        webSetting.setAppCacheEnabled(true);
        webSetting.setDomStorageEnabled(true);
        webSetting.setDefaultTextEncodingName("utf-8");
        webSetting.setAppCacheMaxSize(Long.MAX_VALUE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_t62, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWebView(view);

    }


    // 持续用旧版本的方法
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        System.out.println(isVisibleToUser);
        if (isVisibleToUser) {
            String string = null;
            try {
                HttpTools.url("/UserAppointment/getAll").postAsy(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        System.out.println("Nothing");
                        System.out.println(e.getMessage());
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        JsonObject asJsonObject = new JsonParser().parse(response.body().string()).getAsJsonObject();
                        String data = asJsonObject.get("data").getAsJsonArray().toString();

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                wv.loadUrl("javascript:addingData(" + data + ")");
                            }
                        });

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


}