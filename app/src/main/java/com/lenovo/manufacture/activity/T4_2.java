package com.lenovo.manufacture.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lenovo.manufacture.BaseActivity;
import com.lenovo.manufacture.R;
import com.lenovo.manufacture.adapter.T4RecyclerViewAdapter;
import com.lenovo.manufacture.entity.et4.Commodity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class T4_2 extends BaseActivity {
    @BindView(R.id.rv_t4)
    RecyclerView rv_t4;
    @BindView(R.id.ll_gold)
    LinearLayout ll_gold;

    @BindView(R.id.ll_num)
    LinearLayout ll_num;


    @BindView(R.id.iv_gold)
    ImageView iv_gold;


    @BindView(R.id.iv_num)
    ImageView iv_num;
@BindView(R.id.tv_total_goods_prices)
    TextView tv_total_goods_prices;
    T4RecyclerViewAdapter adapter;
    boolean px1 ;
    boolean px2 ;

    ArrayList<Commodity> commodityArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t4_2);
        super.createToolBar("已购商品");
        ButterKnife.bind(this);
        initViewData();
        iv_gold.setBackgroundResource(R.mipmap.sanjao0002);
        iv_num.setBackgroundResource(R.mipmap.sanjao0002);
        initClickListener();
    }

    private void initClickListener() {

        px1 = true;
        px2 = true;
//单价排序
        ll_gold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (px1){//正序
                    iv_gold.setBackgroundResource(R.mipmap.sanjao0001);
                    iv_num.setBackgroundResource(R.mipmap.sanjao0002);
                    px1 = !px1;
                    Collections.sort(commodityArrayList, new Comparator<Commodity>() {
                        @Override
                        public int compare(Commodity o1, Commodity o2) {
                            return o1.getGold() - o2.getGold();
                        }
                    });
                }else {//反序
                    iv_gold.setBackgroundResource(R.mipmap.sanjao0003);
                    iv_num.setBackgroundResource(R.mipmap.sanjao0002);
                    px1 = !px1;
                    Collections.sort(commodityArrayList, ((o1, o2) -> o2.getGold()-o1.getGold()));
                }

                adapter.notifyDataSetChanged(commodityArrayList);
            }
        });

//数量排序
        ll_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (px2){//正序
                    iv_num.setBackgroundResource(R.mipmap.sanjao0001);
                    iv_gold.setBackgroundResource(R.mipmap.sanjao0002);
                    px2 = !px2;
                    Collections.sort(commodityArrayList, new Comparator<Commodity>() {
                        @Override
                        public int compare(Commodity o1, Commodity o2) {
                            return o1.getNum() - o2.getNum();
                        }
                    });
                }else {//反序
                    iv_num.setBackgroundResource(R.mipmap.sanjao0003);
                    iv_gold.setBackgroundResource(R.mipmap.sanjao0002);
                    px2 = !px2;
                    Collections.sort(commodityArrayList, ((o1, o2) -> o2.getNum()-o1.getNum()));
                }

                adapter.notifyDataSetChanged(commodityArrayList);
            }
        });
    }

    private void initViewData() {

        commodityArrayList = ( ArrayList<Commodity>)(getIntent().getBundleExtra("bundle").getSerializable("purchasedGoodsList"));
        rv_t4.setLayoutManager(new LinearLayoutManager(T4_2.this, LinearLayoutManager.VERTICAL, false));
        adapter = new T4RecyclerViewAdapter();
        adapter.notifyDataSetChanged(commodityArrayList);
        rv_t4.setAdapter(adapter);

        int totalGoodsPrices = 0;
        if (commodityArrayList.size() != 0){
            for (Commodity commodity:
            commodityArrayList ) {
                totalGoodsPrices +=( commodity.getGold()*commodity.getNum());
            }

            tv_total_goods_prices.setText(String.valueOf(totalGoodsPrices));
        }
    }


}