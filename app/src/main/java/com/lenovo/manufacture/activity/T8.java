package com.lenovo.manufacture.activity;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.lenovo.manufacture.BaseActivity;
import com.lenovo.manufacture.R;
import com.lenovo.manufacture.adapter.WorkerAdapter;
import com.lenovo.manufacture.entity.et8.Workers;
import com.lenovo.manufacture.utils.HttpTools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class T8 extends BaseActivity {

    private ArrayList<Workers> workers = new ArrayList<>();
    private RecyclerView recyclerView;
    private WorkerAdapter workerAdapter;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t8);
        super.createToolBar("人事管理");
        initWebView();

        // RecView
        HttpTools.url("/People/search")
                .postAsy(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        System.out.println("FAILED");
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        System.out.println("DONE");
                        String string = response.body().string();
                        System.out.println(string);

                        Map map = new Gson().fromJson(string, Map.class);

                        ArrayList data = (ArrayList) map.get("data");
                        for (Object datum : data) {
                            LinkedTreeMap datum1 = (LinkedTreeMap) datum;
                            String pNname = String.valueOf(datum1.get("peopleName"));
                            String pContent = String.valueOf(datum1.get("content"));
                            workers.add(new Workers(pNname, pContent));
                        }

                        T8.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView = findViewById(R.id.hrView);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                workerAdapter = new WorkerAdapter(workers);
                                recyclerView.setAdapter(workerAdapter);
                            }
                        });


                    }
                });


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint({"SetJavaScriptEnabled"})
    private void initWebView() {

        TWebView webView = new TWebView(this, null);
        ViewGroup viewParent = findViewById(R.id.HRMon);
        viewParent.addView(webView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));

        webView.loadUrl("file:///android_asset/HRMon.html");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                /* mWebView.showLog("test Log"); */
            }
        });

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webView.loadUrl("javascript:changeStep(7)");
            }
        });


        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setBackgroundColor(0);
        webView.requestFocus();
        webView.setInitialScale(25);
//        webView.addJavascriptInterface(new T7.JavaScriptInterface(this), "nativeMethod");
        WebSettings webSetting = webView.getSettings();
        webSetting.setAllowFileAccess(true);
        webSetting.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
//        webSetting.setSupportZoom(true);
//        webSetting.setBuiltInZoomControls(true);
        webSetting.setUseWideViewPort(true);
        webSetting.setSupportMultipleWindows(false);
//        webSetting.setAppCacheEnabled(true);
        webSetting.setDomStorageEnabled(true);
        webSetting.setDefaultTextEncodingName("utf-8");
        webSetting.setAppCacheMaxSize(Long.MAX_VALUE);

    }

    // TODO Waiting For Online
    public void updatePDLines() throws IOException {
        String s = HttpTools.url("http://192.168.1.101:8085/dataInterface/UserProductionLine/getAll").postSync();
    }
}