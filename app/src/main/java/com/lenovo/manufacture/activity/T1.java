package com.lenovo.manufacture.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.reflect.TypeToken;
import com.lenovo.manufacture.BaseActivity;
import com.lenovo.manufacture.R;
import com.lenovo.manufacture.adapter.T1RecyclerViewAdapter;
import com.lenovo.manufacture.entity.et1.Car;
import com.lenovo.manufacture.entity.et1.UserSellOutLog;
import com.lenovo.manufacture.utils.AnalysisComm;
import com.lenovo.manufacture.utils.HttpTools;
import com.lenovo.manufacture.utils.Information;
import com.lenovo.manufacture.utils.Stringtransfor4js;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class T1 extends BaseActivity {

    private String TAG = "T1";
    private TextView tv_total_car_price;
    private TextView tv_car_price1;
    private TextView tv_car_price2;
    private TextView tv_car_price3;
    private TextView tv_car_name1;
    private TextView tv_car_name2;
    private TextView tv_car_name3;
    private boolean px1 = true;
    private boolean px2 = true;
    private boolean px3 = true;
    private RecyclerView rv_t1;
    private WebView wv_echarts;
    private LinearLayout ll_gold;
    private LinearLayout ll_time;
    private LinearLayout ll_num;

    @BindView(R.id.imageView3)
     ImageView imageView3;
    @BindView(R.id.imageView4)
     ImageView imageView4;
    @BindView(R.id.imageView5)
     ImageView imageView5;

    private ArrayList<UserSellOutLog> userSellOutLogList;
    private T1RecyclerViewAdapter adapter;
    private Map<Integer, String> map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t1);
        ButterKnife.bind(this);
        initView();
        initClickListener();
        super.createToolBar("销售报表");

        new AsyncTask<Object, Object, ArrayList<ArrayList>>() {

            @Override
            protected ArrayList<ArrayList> doInBackground(Object... objects) {
                return requeseData();
            }

            @Override
            protected void onPostExecute(ArrayList<ArrayList> lists) {
                super.onPostExecute(lists);
                ArrayList<Car> carList = lists.get(0);
                userSellOutLogList = lists.get(1);
                map = new HashMap();
                for (Car car : carList
                ) {
                    map.put(car.getId(), car.getCarName());
                }
                initViewData(userSellOutLogList, map);
                initEcharts(userSellOutLogList);
            }


        }.execute();
    }

    private void initClickListener() {
        ll_gold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: "+userSellOutLogList);
                if (px1){//正序
                    imageView3.setBackgroundResource(R.mipmap.sanjao0004);
                    px1 = false;
                    Collections.sort(userSellOutLogList, new Comparator<UserSellOutLog>() {
                        @Override
                        public int compare(UserSellOutLog o1, UserSellOutLog o2) {
                            return o1.getGold()-o2.getGold();
                        }
                    });
                }else {
                    imageView3.setBackgroundResource(R.mipmap.sanjao0002);
                    px1 = true;
                    Collections.sort(userSellOutLogList, ((o1, o2) -> o2.getGold()-o1.getGold()));
                }

                initViewData(userSellOutLogList,map);
                Log.d(TAG, "onClick: "+userSellOutLogList);
            }
        });

        ll_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (px2){
                    imageView4.setImageResource(R.mipmap.sanjao0004);
                    px2 = false;
                    Collections.sort(userSellOutLogList, ((o1, o2) -> o1.getTime()-o2.getTime()));
                }else {
                    imageView4.setImageResource(R.mipmap.sanjao0002);
                    px2 = true;
                    Collections.sort(userSellOutLogList, ((o1, o2) -> o2.getTime()-o1.getTime()));
                }
                initViewData(userSellOutLogList,map);
            }
        });

        ll_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (px3){
                    imageView5.setImageResource(R.mipmap.sanjao0004);
                    px3 = false;
                    Collections.sort(userSellOutLogList, ((o1, o2) -> o1.getNum()-o2.getNum()));
                }else {
                    imageView5.setImageResource(R.mipmap.sanjao0002);
                    px3 = true;
                    Collections.sort(userSellOutLogList, ((o1, o2) -> o2.getNum()-o1.getNum()));
                }
                initViewData(userSellOutLogList,map);
            }
        });

//        adapter.notifyDataSetChanged(userSellOutLogList);
//        initViewData(userSellOutLogList,map);
    }


    private void initEcharts(ArrayList<UserSellOutLog> userSellOutLogList) {
        List<String> dateList = new ArrayList<>();
        List<String> priceList = new ArrayList<>();
/*        for (int i = 0; i <userSellOutLogList.size() ; i++) {
            UserSellOutLog userSellOutLog = userSellOutLogList.get(i);
            userSellOutLog.getTime();
            userSellOutLog.getGold();
        }*/
        Stringtransfor4js stringtransfor4js = new Stringtransfor4js();

        dateList.add(stringtransfor4js.transforJsString("8月1日"));
        dateList.add(stringtransfor4js.transforJsString("8月2日"));
        Log.d(TAG, "initEcharts: " + dateList.get(0));
        dateList.add(stringtransfor4js.transforJsString("8月3日"));
        dateList.add(stringtransfor4js.transforJsString("8月4日"));
        dateList.add(stringtransfor4js.transforJsString("8月5日"));
        priceList.add("150");
        priceList.add("450");
        priceList.add("350");
        priceList.add("110");
        priceList.add("320");
/*        for (UserSellOutLog u:userSellOutLogList
             ) {

        }*/
        wv_echarts.loadUrl("file:///android_asset/t1echarts.html");
        wv_echarts.getSettings().setAllowFileAccess(true);
        wv_echarts.getSettings().setJavaScriptEnabled(true);
        wv_echarts.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {

                wv_echarts.loadUrl("javascript:createLineChart(" + dateList + "," + priceList + ")");
                super.onPageFinished(view, url);
            }
        });

    }

    private void initViewData(ArrayList<UserSellOutLog> userSellOutLogList, Map<Integer, String> map) {
        int car1Price = 0;
        int car2Price = 0;
        int car3Price = 0;
        int totalCarPrice = 0;

//        计算各种车型的价格
        for (int i = 0; i < userSellOutLogList.size(); i++) {
            UserSellOutLog userSellOutLog = userSellOutLogList.get(i);
            switch (userSellOutLog.getCarId()) {
                case 1:
                    car1Price += (userSellOutLog.getNum() * userSellOutLog.getGold());
                    break;
                case 2:
                    car2Price += (userSellOutLog.getNum() * userSellOutLog.getGold());
                    break;
                case 3:
                    car3Price += (userSellOutLog.getNum() * userSellOutLog.getGold());
                    break;
            }
        }

        totalCarPrice = car1Price + car2Price + car3Price;
        tv_car_name1.setText(map.get(1) + "价格");
        tv_car_name2.setText(map.get(2) + "价格");
        tv_car_name3.setText(map.get(3) + "价格");
        tv_car_price1.setText(String.valueOf(car1Price));
        tv_car_price2.setText(String.valueOf(car2Price));
        tv_car_price3.setText(String.valueOf(car3Price));
        tv_total_car_price.setText(String.valueOf(totalCarPrice));

        adapter = new T1RecyclerViewAdapter();
        adapter.notifyDataSetChanged(map, userSellOutLogList);
        rv_t1.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rv_t1.setAdapter(adapter);


    }


    private void initView() {
        tv_total_car_price = findViewById(R.id.tv_total_car_price);
        tv_car_price1 = findViewById(R.id.tv_car_price1);
        tv_car_price2 = findViewById(R.id.tv_car_price2);
        tv_car_price3 = findViewById(R.id.tv_car_price3);
        tv_car_name1 = findViewById(R.id.tv_car_name1);
        tv_car_name2 = findViewById(R.id.tv_car_name2);
        tv_car_name3 = findViewById(R.id.tv_car_name3);

        rv_t1 = findViewById(R.id.rv_t1);
        wv_echarts = findViewById(R.id.wv_echarts);
        ll_gold = findViewById(R.id.ll_price);
        ll_time = findViewById(R.id.ll_time_of_sell);
        ll_num = findViewById(R.id.ll_quantity_of_sold);
    }


    /**
     * 请求出售车型
     * 请求售出数据
     *
     * @return 出售车型及售出数据集合
     */
    private ArrayList requeseData() {
        try {
            String car = HttpTools.url("/Car/getAll").postSync();
            Information carInfos = AnalysisComm.getInfos(car, new TypeToken<ArrayList<Car>>() {
            }.getType());
            ArrayList<Car> carList = (ArrayList<Car>) carInfos.getObject();

            String UserSellOutLog = HttpTools.url("/UserSellOutLog/getAll").postSync();
            Information UserSellOutLogInfos = AnalysisComm.getInfos(UserSellOutLog, new TypeToken<ArrayList<UserSellOutLog>>() {
            }.getType());
            ArrayList<UserSellOutLog> UserSellOutLogList = (ArrayList<UserSellOutLog>) UserSellOutLogInfos.getObject();

            ArrayList list = new ArrayList();
            list.add(carList);
            list.add(UserSellOutLogList);
            Log.d(TAG, "requeseData: " + list);
            return list;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}