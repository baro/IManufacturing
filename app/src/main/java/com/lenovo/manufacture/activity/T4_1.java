package com.lenovo.manufacture.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.reflect.TypeToken;
import com.lenovo.manufacture.BaseActivity;
import com.lenovo.manufacture.R;
import com.lenovo.manufacture.adapter.T4RecyclerViewAdapter;
import com.lenovo.manufacture.entity.et4.Commodity;
import com.lenovo.manufacture.entity.et4.Part;
import com.lenovo.manufacture.entity.et4.SuppierList;
import com.lenovo.manufacture.utils.AnalysisComm;
import com.lenovo.manufacture.utils.HttpTools;
import com.lenovo.manufacture.utils.Information;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class T4_1 extends BaseActivity {
    private static final String TAG = "baroknight";
    @BindView(R.id.shopping_car)
    ImageButton shopping_car;
    int CosteffectiveMax;
    Commodity commodityMax;

    @BindView(R.id.rv_t4)
    RecyclerView rv_t4;

    @BindView(R.id.ll_gold)
    LinearLayout ll_gold;

    @BindView(R.id.ll_num)
    LinearLayout ll_num;


    @BindView(R.id.iv_gold)
    ImageView iv_gold;


    @BindView(R.id.iv_num)
    ImageView iv_num;

    @BindView(R.id.btn_purchase_materials)
    Button btn_purchase_materials;

    T4RecyclerViewAdapter adapter;

    ArrayList<Commodity> commodityArrayList = new ArrayList<>();
    ArrayList<Commodity> purchasedGoodsList = new ArrayList<>();
    ArrayList<Commodity> costEffectiveList = new ArrayList<>();
    boolean px1 ;
    boolean px2 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t4_1);
        super.createToolBar("商品列表");
        ButterKnife.bind(this);
        iv_gold.setBackgroundResource(R.mipmap.sanjao0002);
        iv_num.setBackgroundResource(R.mipmap.sanjao0002);
        initClickListener();

        new AsyncTask<Object, Object, ArrayList<Commodity>>() {

            @Override
            protected ArrayList<Commodity> doInBackground(Object... objects) {
                return requeseData();
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                rv_t4.setLayoutManager(new LinearLayoutManager(T4_1.this, LinearLayoutManager.VERTICAL, false));
                adapter = new T4RecyclerViewAdapter();
                adapter.notifyDataSetChanged(new ArrayList<Commodity>());
                rv_t4.setAdapter(adapter);
            }

            @Override
            protected void onPostExecute(ArrayList<Commodity> lists) {
                super.onPostExecute(lists);
                commodityArrayList = lists;
                adapter.notifyDataSetChanged(lists);

                costEffectiveList.addAll(lists);
                Collections.sort(costEffectiveList, ((o1, o2) ->( ( o1.getGold()/o1.getNum())-(o2.getGold()/o2.getNum()))));
            }


        }.execute();
    }


    private void initClickListener() {
        px1 = true;
        px2 = true;




//单价排序
        ll_gold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (px1){//正序
                    iv_gold.setBackgroundResource(R.mipmap.sanjao0001);
                    iv_num.setBackgroundResource(R.mipmap.sanjao0002);
                    px1 = !px1;
                    Collections.sort(commodityArrayList, new Comparator<Commodity>() {
                        @Override
                        public int compare(Commodity o1, Commodity o2) {
                            return o1.getGold() - o2.getGold();
                        }
                    });
                }else {//反序
                    iv_gold.setBackgroundResource(R.mipmap.sanjao0003);
                    iv_num.setBackgroundResource(R.mipmap.sanjao0002);
                    px1 = !px1;
                    Collections.sort(commodityArrayList, ((o1, o2) -> o2.getGold()-o1.getGold()));
                }

                adapter.notifyDataSetChanged(commodityArrayList);
            }
        });

//数量排序
        ll_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (px2){//正序
                    iv_num.setBackgroundResource(R.mipmap.sanjao0001);
                    iv_gold.setBackgroundResource(R.mipmap.sanjao0002);
                    px2 = !px2;
                    Collections.sort(commodityArrayList, new Comparator<Commodity>() {
                        @Override
                        public int compare(Commodity o1, Commodity o2) {
                            return o1.getNum() - o2.getNum();
                        }
                    });
                }else {//反序
                    iv_num.setBackgroundResource(R.mipmap.sanjao0003);
                    iv_gold.setBackgroundResource(R.mipmap.sanjao0002);
                    px2 = !px2;
                    Collections.sort(commodityArrayList, ((o1, o2) -> o2.getNum()-o1.getNum()));
                }

                adapter.notifyDataSetChanged(commodityArrayList);
            }
        });

//        购物车图标跳转监听
        shopping_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(T4_1.this, T4_2.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("purchasedGoodsList",(Serializable)purchasedGoodsList);
                intent.putExtra("bundle",bundle);
                startActivity(intent);
                Log.d(TAG, "onClick: " + CosteffectiveMax);
                Log.d(TAG, "onClick: " + commodityMax);
            }
        });

//          购买材料按钮的监听事件
        btn_purchase_materials.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purchasedGoodsList.add(costEffectiveList.remove(0));
            }
        });

    }


    private ArrayList requeseData() {
        try {
//            获取题目需要的类的集合
            ArrayList<Commodity> commodityList = new ArrayList<>();


//           获取所有id、商品名称的集合
            String part = HttpTools.url("/Part/getAll").postSync();
            Information partInfos = AnalysisComm.getInfos(part, new TypeToken<ArrayList<Part>>() {
            }.getType());
            ArrayList<Part> partArrayList = (ArrayList<Part>) partInfos.getObject();
            Log.d(TAG, "partArrayList: " + partArrayList);


            for (int i = 0; i < partArrayList.size(); i++) {
                int id = partArrayList.get(i).getId();
                //                根据id获取单价、数量
                String sl = HttpTools.url("/SuppierList/search").addParam("partId", String.valueOf(id)).postSync();
                Information infos = AnalysisComm.getInfos(sl, new TypeToken<ArrayList<SuppierList>>() {
                }.getType());
                ArrayList<SuppierList> suppierListArrayList = (ArrayList<SuppierList>) infos.getObject();
                Log.d(TAG, "suppierListArrayList: " + suppierListArrayList);
                CosteffectiveMax = i == 0 ? suppierListArrayList.get(i).getGold() / suppierListArrayList.get(i).getNum() : CosteffectiveMax;

                for (int j = 0; j < suppierListArrayList.size(); j++) {

                    //                根据id获取供货商名称
                    String Suppier = HttpTools.url("/Suppier/search").addParam("id", String.valueOf(suppierListArrayList.get(j).getSuppierId())).postSync();
                    Information SuppierInfos = AnalysisComm.getInfos(Suppier, new TypeToken<ArrayList<com.lenovo.manufacture.entity.et4.Suppier>>() {
                    }.getType());
                    ArrayList<com.lenovo.manufacture.entity.et4.Suppier> SuppierArrayList = (ArrayList<com.lenovo.manufacture.entity.et4.Suppier>) SuppierInfos.getObject();
                    Log.d(TAG, "SuppierArrayList: " + SuppierArrayList);

//            获取题目需要的类的集合
                    Commodity commodity = new Commodity();
                    commodity.setId(id);
                    commodity.setPartName(partArrayList.get(i).getPartName());
                    commodity.setSuppierName(SuppierArrayList.get(0).getContent());
                    commodity.setGold(suppierListArrayList.get(j).getGold());
                    commodity.setNum(suppierListArrayList.get(j).getNum());
                    commodityList.add(commodity);

                    if (i == 0 & j == 0) {
                        commodityMax = commodity;
                    }
                    int Costeffective = commodity.getGold() / commodity.getNum();
                    if (CosteffectiveMax > Costeffective) {
                        CosteffectiveMax = Costeffective;
                        commodityMax = commodity;
                    }
                }
            }
            Log.d(TAG, "commodityList: " + commodityList.size());
            return commodityList;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}