package com.lenovo.manufacture.activity;

import android.os.Build;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.lenovo.manufacture.BaseActivity;
import com.lenovo.manufacture.R;
import com.lenovo.manufacture.adapter.CarCusAdapter;
import com.lenovo.manufacture.fragment.Fragment_T61;
import com.lenovo.manufacture.fragment.Fragment_T62;

import java.util.ArrayList;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

public class T6 extends BaseActivity {

    private ViewPager viewPager;
    private CarCusAdapter carCusAdapter;
    private TabLayout tabLayout;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t6);
        super.createToolBar("车辆定制");

        // Init View
        viewPager = findViewById(R.id.ViewPager);
        tabLayout = findViewById(R.id.tabLayout);

        ArrayList<Fragment> views = new ArrayList<>();
        views.add(new Fragment_T61());
        views.add(new Fragment_T62());

        carCusAdapter = new CarCusAdapter(views, getSupportFragmentManager());

        viewPager.setAdapter(carCusAdapter);

        // Setup with VP
        tabLayout.setupWithViewPager(viewPager);

    }


}