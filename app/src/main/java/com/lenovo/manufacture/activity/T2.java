package com.lenovo.manufacture.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.lenovo.manufacture.BaseActivity;
import com.lenovo.manufacture.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class T2 extends BaseActivity {

    @BindView(R.id.ib_close)
    ImageButton ib_close;
    @BindView(R.id.btn_cold)
    Button btn_cold;
    @BindView(R.id.btn_warm)
    Button btn_warm;
    @BindView(R.id.iv_cold)
    ImageView iv_cold;
    @BindView(R.id.iv_warm)
    ImageView iv_warm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t2);
        super.createToolBar("工厂环境");
        ButterKnife.bind(this);

        initView();
        
        initClickListener();

    }

    private void initClickListener() {
//        关闭车间空调,冷风提示灯和暖风提示灯置灰
        ib_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_cold.setImageResource(R.mipmap.cold_wind0002);
                iv_warm.setImageResource(R.mipmap.warm_wind0002);
            }
        });

        //        车间空调开启冷风模式,冷风提示灯亮起,暖风提示灯置灰
        btn_warm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_cold.setImageResource(R.mipmap.cold_wind0001);
                iv_warm.setImageResource(R.mipmap.warm_wind0002);
            }
        });

        //        车间空调开启暖风模式,暖风提示灯亮起,冷风提示灯置灰
        btn_cold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_cold.setImageResource(R.mipmap.cold_wind0002);
                iv_warm.setImageResource(R.mipmap.warm_wind0001);
            }
        });

    }

    private void initView() {

    }
}