package com.lenovo.manufacture.entity.et1;

/**
 * @author Mr-Jies
 * @version 1.0
 * @date 2020/6/25 11:36
 */
public class UserSellOutLog {
    private int id;
    private int userWorkId;
    private int carId;
    private int gold;
    private int time;
    private int num;

    @Override
    public String toString() {
        return "UserSellOutLog{" +
                "id=" + id +
                ", userWorkId=" + userWorkId +
                ", carId=" + carId +
                ", gold=" + gold +
                ", time=" + time +
                ", num=" + num +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserWorkId() {
        return userWorkId;
    }

    public void setUserWorkId(int userWorkId) {
        this.userWorkId = userWorkId;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public UserSellOutLog() {
    }

    public UserSellOutLog(int id, int userWorkId, int carId, int gold, int time, int num) {
        this.id = id;
        this.userWorkId = userWorkId;
        this.carId = carId;
        this.gold = gold;
        this.time = time;
        this.num = num;
    }
}
