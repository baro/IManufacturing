package com.lenovo.manufacture.entity.et3;

/**
 * @author Mr-Jies
 * @version 1.0
 * @date 2020/6/25 13:01
 */
public class UserProductionLine {
    private int id;
    private int userWorkId;
    private int stageId;
    private int productionLineId;
    private int type;
    private int position;
    private int isAI;

    @Override
    public String toString() {
        return "UserProductionLine{" +
                "id=" + id +
                ", userWorkId=" + userWorkId +
                ", stageId=" + stageId +
                ", productionLineId=" + productionLineId +
                ", type=" + type +
                ", position=" + position +
                ", isAI=" + isAI +
                '}';
    }

    public UserProductionLine() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserWorkId() {
        return userWorkId;
    }

    public void setUserWorkId(int userWorkId) {
        this.userWorkId = userWorkId;
    }

    public int getStageId() {
        return stageId;
    }

    public void setStageId(int stageId) {
        this.stageId = stageId;
    }

    public int getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(int productionLineId) {
        this.productionLineId = productionLineId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getIsAI() {
        return isAI;
    }

    public void setIsAI(int isAI) {
        this.isAI = isAI;
    }

    public UserProductionLine(int id, int userWorkId, int stageId, int productionLineId, int type, int position, int isAI) {
        this.id = id;
        this.userWorkId = userWorkId;
        this.stageId = stageId;
        this.productionLineId = productionLineId;
        this.type = type;
        this.position = position;
        this.isAI = isAI;
    }
}
