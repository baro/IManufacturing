package com.lenovo.manufacture.entity.et8;

/*
 * Date: 6/26/20
 * Author: EggOxygen
 */
public class Workers {
    private String workerName;
    private String workerType;

    public Workers(String workerName, String workerType) {
        this.workerName = workerName;
        this.workerType = workerType;
    }

    public String getWorkerName() {
        return workerName;
    }

    public void setWorkerName(String workerName) {
        this.workerName = workerName;
    }

    public String getWorkerType() {
        return workerType;
    }

    public void setWorkerType(String workerType) {
        this.workerType = workerType;
    }
}
