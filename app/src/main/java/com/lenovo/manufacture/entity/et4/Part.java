package com.lenovo.manufacture.entity.et4;

public class Part {
    private int id;
    private String partName;
    private String content;
    private int area;
    private String icon;

    @Override
    public String toString() {
        return "Part{" +
                "id=" + id +
                ", partName='" + partName + '\'' +
                ", content='" + content + '\'' +
                ", area=" + area +
                ", icon='" + icon + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Part(int id, String partName, String content, int area, String icon) {
        this.id = id;
        this.partName = partName;
        this.content = content;
        this.area = area;
        this.icon = icon;
    }
}
