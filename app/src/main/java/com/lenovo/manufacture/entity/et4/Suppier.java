package com.lenovo.manufacture.entity.et4;

public class Suppier {
    private int id;
    private String suppierName;
    private String content;

    @Override
    public String toString() {
        return "Suppier{" +
                "id=" + id +
                ", suppierName='" + suppierName + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public Suppier(int id, String suppierName, String content) {
        this.id = id;
        this.suppierName = suppierName;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSuppierName() {
        return suppierName;
    }

    public void setSuppierName(String suppierName) {
        this.suppierName = suppierName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
