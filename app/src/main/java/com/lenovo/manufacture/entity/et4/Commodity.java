package com.lenovo.manufacture.entity.et4;

import java.io.Serializable;

public class Commodity implements Serializable {
    private int id;
    private String partName;
    private String suppierName;
    private int gold;
    private int num;

    @Override
    public String toString() {
        return "Commodity{" +
                "id=" + id +
                ", partName='" + partName + '\'' +
                ", suppierName='" + suppierName + '\'' +
                ", gold=" + gold +
                ", num=" + num +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getSuppierName() {
        return suppierName;
    }

    public void setSuppierName(String suppierName) {
        this.suppierName = suppierName;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
