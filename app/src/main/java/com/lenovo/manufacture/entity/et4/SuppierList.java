package com.lenovo.manufacture.entity.et4;

public class SuppierList {
    private int id;
    private int suppierId;
    private int partId;
    private int gold;
    private int num;

    public SuppierList(int id, int suppierId, int partId, int gold, int num) {
        this.id = id;
        this.suppierId = suppierId;
        this.partId = partId;
        this.gold = gold;
        this.num = num;
    }

    @Override
    public String toString() {
        return "SuppierList{" +
                "id=" + id +
                ", suppierId=" + suppierId +
                ", partId=" + partId +
                ", gold=" + gold +
                ", num=" + num +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSuppierId() {
        return suppierId;
    }

    public void setSuppierId(int suppierId) {
        this.suppierId = suppierId;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
