package com.lenovo.manufacture.entity.et1;

/**
 * @author Mr-Jies
 * @version 1.0
 * @date 2020/6/25 9:48
 */
public class Car {
    private int id;
    private String carName;
    private String content;

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", carName='" + carName + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public Car() {
    }

    public Car(int id, String carName, String content) {
        this.id = id;
        this.carName = carName;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
