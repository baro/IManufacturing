package com.lenovo.manufacture.entity.et2;

/**
 * @author Mr-Jies
 * @version 1.0
 * @date 2020/6/25 12:15
 */
public class UserWorkEnvironmental {
    private String id;
    private String day;
    private String lightSwitch;
    private String acOnOff;
    private String beam;
    private String workshopTemp;
    private String outTemp;
    private String power;
    private String powerConsume;
    private String time;

    public UserWorkEnvironmental() {
    }

    @Override
    public String toString() {
        return "UserWorkEnvironmental{" +
                "id='" + id + '\'' +
                ", day='" + day + '\'' +
                ", lightSwitch='" + lightSwitch + '\'' +
                ", acOnOff='" + acOnOff + '\'' +
                ", beam='" + beam + '\'' +
                ", workshopTemp='" + workshopTemp + '\'' +
                ", outTemp='" + outTemp + '\'' +
                ", power='" + power + '\'' +
                ", powerConsume='" + powerConsume + '\'' +
                ", time='" + time + '\'' +
                '}';
    }

    public UserWorkEnvironmental(String id, String day, String lightSwitch, String acOnOff, String beam, String workshopTemp, String outTemp, String power, String powerConsume, String time) {
        this.id = id;
        this.day = day;
        this.lightSwitch = lightSwitch;
        this.acOnOff = acOnOff;
        this.beam = beam;
        this.workshopTemp = workshopTemp;
        this.outTemp = outTemp;
        this.power = power;
        this.powerConsume = powerConsume;
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getLightSwitch() {
        return lightSwitch;
    }

    public void setLightSwitch(String lightSwitch) {
        this.lightSwitch = lightSwitch;
    }

    public String getAcOnOff() {
        return acOnOff;
    }

    public void setAcOnOff(String acOnOff) {
        this.acOnOff = acOnOff;
    }

    public String getBeam() {
        return beam;
    }

    public void setBeam(String beam) {
        this.beam = beam;
    }

    public String getWorkshopTemp() {
        return workshopTemp;
    }

    public void setWorkshopTemp(String workshopTemp) {
        this.workshopTemp = workshopTemp;
    }

    public String getOutTemp() {
        return outTemp;
    }

    public void setOutTemp(String outTemp) {
        this.outTemp = outTemp;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getPowerConsume() {
        return powerConsume;
    }

    public void setPowerConsume(String powerConsume) {
        this.powerConsume = powerConsume;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
