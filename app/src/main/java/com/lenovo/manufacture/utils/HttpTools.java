package com.lenovo.manufacture.utils;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author Mr-Jies
 * @version 1.0
 * @date 2020/6/24 19:08
 */
public class HttpTools {
    private static OkHttpClient client = OkHttpUtils.getInstance();

//    private static final String BASE_URL = "http://192.168.191.3:8085/dataInterface";
    private static final String BASE_URL = "http://192.168.1.101:8085/dataInterface";

    public static Builder url(String url) {
        return new Builder(url);
    }

    public static class Builder {
        private String url;
        private Map<String, String> params;
        Request.Builder reqBuilder;

        Builder(String url) {
            this.url = BASE_URL + url;
            params = new HashMap<>();
            reqBuilder = new Request.Builder();

        }

        // 添加参数
        public Builder addParam(String key, String value) {
            params.put(key, value);
            return this;
        }

        // 添加头
        public Builder addHeader(String key, String value) {
            reqBuilder.addHeader(key, value);
            return this;
        }

        /**
         * 同步的Get请求
         *
         * @return responseStr
         * @throws IOException
         */
        public String getSync() throws IOException {
            // 创建OKHttpClient对象
            // 创建一个Request
            final Request request = new Request.Builder()
                    .url(url)
                    .build();
            Call call = client.newCall(request);
            // 返回值为response
            Response response = call.execute();
            // 将response转化成String
            String responseStr = response.body().string();
            return responseStr;
        }

        /**
         * 同步Post请求
         * @return
         */
        public String postSync() throws IOException {
            FormBody.Builder builder = new FormBody.Builder();
//            Gson gson = new Gson();
//            String s = gson.toJson(params);
//            RequestBody requestBody = FormBody.create(MediaType.parse("application/x-www-form-urlencoded;charset=utf-8"), s);

            addParams(builder);
            return client.newCall(reqBuilder.url(url)
//                    .post(requestBody)
                    .post(builder.build())
                    .build())
                    .execute().body().string();

        }

        /**
         * 异步get请求
         * @param callback
         */
        public void getAsyn(Callback callback){
            FormBody.Builder builder = new FormBody.Builder();
            addParams(builder);
            client.newCall(reqBuilder.url(url)
                    .post(builder.build())
                    .build())
                    .enqueue(callback);
        }

        /**
         * 异步post请求
         * @param callback
         */
        public void postAsy(Callback callback){
            FormBody.Builder builder = new FormBody.Builder();
//            Gson gson = new Gson();
//            String s = gson.toJson(params);
//            RequestBody requestBody = FormBody.create(MediaType.parse("application/x-www-form-urlencoded;charset=utf-8"), s);
            addParams(builder);
            client.newCall(reqBuilder.url(url)
//                    .post(requestBody)
                    .post(builder.build())
                    .build())
                    .enqueue(callback);
        }

        /**
         * 添加参数
         * @param builder
         */
        private void addParams(FormBody.Builder builder) {
            if (!params.isEmpty()) {
                for (Map.Entry<String,String> map:params.entrySet()) {
                    String key = map.getKey();
                    String value ;
                    if (map.getValue() == null){
                        value = "";
                    }else {
                        value = map.getValue();
                    }
                    builder.add(key,value);
                }
            }
        }
    }
}
