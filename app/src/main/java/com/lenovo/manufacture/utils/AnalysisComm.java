package com.lenovo.manufacture.utils;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.lang.reflect.Type;

/**
 * @author Mr-Jies
 * @version 1.0
 * @date 2020/6/25 12:44
 */
public class AnalysisComm {
    private static String TAG = "AnalysisComm";

    public static Information getInfos(String json, Type type) {
        Information information = new Information();
        JsonObject asJsonObject = new JsonParser().parse(json).getAsJsonObject();
        int status = asJsonObject.get("status").getAsInt();
        String message = asJsonObject.get("message").getAsString();
        Log.d(TAG, "getInfos: " + status);
        information.setStatus(status);
        information.setMessage(message);
        if (status == 200) {
            Gson gson = new Gson();
            JsonArray data = asJsonObject.get("data").getAsJsonArray();
            String s = data.toString();
            Object o = gson.fromJson(s, type);
            information.setObject(o);
            Log.d(TAG, "getInfos: "+information);
            return information;
        } else {
            return information;
        }

    }

    public static Information getInfoByOne(String json,Class clazz) {
        Information information = new Information();
        JsonObject asJsonObject = new JsonParser().parse(json).getAsJsonObject();
        int status = asJsonObject.get("status").getAsInt();
        String message = asJsonObject.get("message").getAsString();
        Log.d(TAG, "getInfoByOne: " + status);
        information.setStatus(status);
        information.setMessage(message);
        if (status == 200) {
            Gson gson = new Gson();
            JsonObject data = asJsonObject.get("data").getAsJsonObject();
            String s = data.toString();
            Object o = gson.fromJson(s, clazz);
            information.setObject(o);
            Log.d(TAG, "getInfos: "+information);
            return information;
        } else {
            return information;
        }

    }
}
