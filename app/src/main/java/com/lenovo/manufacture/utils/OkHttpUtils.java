package com.lenovo.manufacture.utils;

import okhttp3.OkHttpClient;

/**
 * @author Mr-Jies
 * @version 1.0
 * @date 2020/6/24 19:06
 */
public class OkHttpUtils {
    private static OkHttpClient instance;

    private OkHttpUtils(){}

    public static OkHttpClient getInstance() {
        if (instance==null){
            synchronized (OkHttpUtils.class){
                if (instance ==null){
                    instance = new OkHttpClient();
                }
            }
        }
        return instance;
    }
}

