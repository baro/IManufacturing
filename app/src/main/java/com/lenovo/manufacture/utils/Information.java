package com.lenovo.manufacture.utils;

/**
 * @author Mr-Jies
 * @version 1.0
 * @date 2020/6/25 10:20
 */
public class Information {
    private int status;
    private String message;
    private Object object;

    @Override
    public String toString() {
        return "Information{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", object=" + object +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public Information() {
    }

    public Information(int status, String message, Object object) {
        this.status = status;
        this.message = message;
        this.object = object;
    }
}
