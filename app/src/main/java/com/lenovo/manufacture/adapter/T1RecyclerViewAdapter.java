package com.lenovo.manufacture.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lenovo.manufacture.R;
import com.lenovo.manufacture.entity.et1.UserSellOutLog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class T1RecyclerViewAdapter extends RecyclerView.Adapter<T1RecyclerViewAdapter.T1Holder> {
   private Map<Integer,String> map;
    private  ArrayList<UserSellOutLog>userSellOutLogList;

    public void notifyDataSetChanged(Map<Integer,String> map, ArrayList<UserSellOutLog>userSellOutLogList) {
        this.map = map;
        this.userSellOutLogList = userSellOutLogList;
        super.notifyDataSetChanged();
    }

    public void notifyDataSetChanged( ArrayList<UserSellOutLog>userSellOutLogList) {
        this.userSellOutLogList = userSellOutLogList;
        super.notifyDataSetChanged();
    }
    @NonNull
    @Override
    public T1Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_t1,parent,false);
        return new T1Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull T1Holder holder, int position) {
       UserSellOutLog userSellOutLog = userSellOutLogList.get(position);

       Date date = new Date();
        date.setTime(userSellOutLog.getTime());

        holder.tv_name.setText(map.get(userSellOutLog.getCarId()));
        holder.tv_price.setText(String.valueOf(userSellOutLog.getGold()));
        holder.tv_time_of_sell.setText(new SimpleDateFormat("yyyy/MM/dd HH:mm").format(date));
        holder.tv_quantity_of_sold.setText(String.valueOf(userSellOutLog.getNum()));

    }

    @Override
    public int getItemCount() {
        return userSellOutLogList.size();
    }

    class T1Holder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private TextView tv_price;
        private TextView tv_time_of_sell;
        private TextView tv_quantity_of_sold;
        public T1Holder(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_time_of_sell = itemView.findViewById(R.id.tv_time_of_sell);
            tv_quantity_of_sold = itemView.findViewById(R.id.tv_quantity_of_sold);
        }


    }
}
