package com.lenovo.manufacture.adapter;

import android.view.View;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

/*
 * Date: 6/25/20
 * Author: EggOxygen
 */
public class CarCusAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> arrayList;
    final String[] titles = {"车辆定制", "订单管理"};

    public CarCusAdapter(ArrayList<Fragment> arrayList, FragmentManager fragmentManager) {
        super(fragmentManager,BEHAVIOR_SET_USER_VISIBLE_HINT);
        this.arrayList = arrayList;
    }


    @Override
    public int getCount() {
        return arrayList.size();
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return arrayList.get(position);
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

}
