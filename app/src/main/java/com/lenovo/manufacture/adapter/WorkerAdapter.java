package com.lenovo.manufacture.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lenovo.manufacture.R;
import com.lenovo.manufacture.entity.et8.Workers;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/*
 * Date: 6/26/20
 * Author: EggOxygen
 */
public class WorkerAdapter extends RecyclerView.Adapter<WorkerAdapter.ViewHolder> {

    private ArrayList<Workers> workers;

    public WorkerAdapter(ArrayList<Workers> workers) {
        this.workers = workers;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_t8_hr, parent, false);
        return new ViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Workers workers = this.workers.get(position);
        holder.workerName.setText(workers.getWorkerName());
        holder.workerType.setText(workers.getWorkerType());

    }


    @Override
    public int getItemCount() {
        return workers.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView workerName;
        TextView workerType;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            workerName = itemView.findViewById(R.id.hr_list_name);
            workerType = itemView.findViewById(R.id.hr_list_work);
        }
    }

}
