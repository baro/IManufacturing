package com.lenovo.manufacture.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lenovo.manufacture.R;
import com.lenovo.manufacture.entity.et4.Commodity;

import java.util.ArrayList;

public class T4RecyclerViewAdapter extends RecyclerView.Adapter<T4RecyclerViewAdapter.T4Holder> {
    private ArrayList<Commodity> commodityArrayList;

    public void notifyDataSetChanged( ArrayList<Commodity> commodityArrayList) {
        this.commodityArrayList = commodityArrayList;
        super.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public T4Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_t4,parent,false);
        return new T4Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull T4Holder holder, int position) {
       Commodity commodity = commodityArrayList.get(position);

        holder.tv_id.setText(String.valueOf(commodity.getId()));
         holder.tv_name.setText(commodity.getPartName());
         holder.tv_supplier.setText(commodity.getSuppierName());
         holder.tv_gold.setText(String.valueOf(commodity.getGold()));
         holder.tv_num.setText(String.valueOf(commodity.getNum()));




    }

    @Override
    public int getItemCount() {
        return commodityArrayList.size();
    }

    class T4Holder extends RecyclerView.ViewHolder {

        private TextView tv_id;
        private TextView tv_name;
        private TextView tv_supplier;
        private TextView tv_gold;
        private TextView tv_num;
        public T4Holder(@NonNull View itemView) {
            super(itemView);
            tv_id = itemView.findViewById(R.id.tv_id);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_supplier = itemView.findViewById(R.id.tv_supplier);
            tv_gold = itemView.findViewById(R.id.tv_gold);
            tv_num = itemView.findViewById(R.id.tv_num);
        }


    }
}
